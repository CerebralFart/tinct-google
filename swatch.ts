import {Color, Swatch} from "tinct";

const google = new Swatch({
	name: 'google',
	colors: {
		gray: new Color("#727272"),
		red: new Color("#ea4335"),
		yellow: new Color("#fbbc05"),
		green: new Color("#34a853"),
		blue: new Color("#4285f4"),
	},
	shades: {}
});

export default google;
